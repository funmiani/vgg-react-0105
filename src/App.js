import React from 'react';
// import logo from './logo.svg';
import './App.css';
import Nav from '../src/Components/Nav';
import Header from './Components/Header';
import Footer from './Components/footer';
import Flex from './Components/FlexItems';


function App() {
  return (
    <div className="App">
      <Nav/>
      <Header/>
      <Flex/>
      <Flex/>
      <Footer/>
    </div>
  );
}

export default App;
